<?php

use App\Http\Controllers\BranchController;
use App\Http\Controllers\Client\AdController;
use App\Http\Controllers\Client\CategoryController;
use App\Http\Controllers\Client\HomeController;
use App\Http\Controllers\Client\InsightControler;
use App\Http\Controllers\Client\ListingController;
use App\Http\Controllers\Client\ProfileController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use PharIo\Version\PreReleaseSuffix;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth:sanctum')->group(function () {

    // Route::get('/', function () {
    //     return view('app.client');
    // });

});

Route::get('/', function () {
    return view('client.index');
});


Route::get('/categories',[CategoryController::class,'index'])->name('client.category.index');
Route::get('/home',[HomeController::class,'index'])->name('client.home.index');
Route::get('/ad',[ListingController::class,'index'])->name('client.listing.index');


Route::group(['prefix' => 'insights'], function () {
    Route::get('', [InsightControler::class, 'index'])->name('client.insight.index');
});

Route::group(['prefix' => 'insights'], function () {
    Route::get('', [InsightControler::class, 'index'])->name('client.insight.index');
});

Route::group(['prefix' => 'profile'], function () {
    Route::get('', [ProfileController::class, 'index'])->name('client.profile.index');
});

Route::group(['prefix' => 'ads'], function () {
    Route::get('', [AdController::class, 'index'])->name('client.ads.index');
    Route::get('/create', [AdController::class, 'create'])->name('client.ads.create');
});