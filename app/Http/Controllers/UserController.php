<?php

namespace App\Http\Controllers;

use Exception;
use DataTables;
use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    //

    public function index()
    {
        // if (!auth()->user()->can('users.view')) {
        //     abort(403, 'Unauthorized action.');
        // }
        // auth()->user()->assignRole('Super Admin');
        $users =User::with('roles')->get();
        // $users =User::all();
        return Inertia::render('Admin/User/Index',compact('users'));
    }

    public function create()
    {
        // if (!auth()->user()->can('users.add')) {
        //     abort(403, 'Unauthorized action.');
        // }
        $all_roles=Role::all();
        return Inertia::render('Admin/User/Create',compact('all_roles'));
    }

    public function edit($id)
    {

        // if (!auth()->user()->can('users.edit')) {
        //     abort(403, 'Unauthorized action.');
        // }

        $user = User::where('id',$id)->firstOrFail();

        return Inertia::render('Users/Edit',compact('user'));
    }

    public function store(Request $request)
    {
        // if (!auth()->user()->can('users.add')) {
        //     abort(403, 'Unauthorized action.');
        // }
       $request->validate([
            'name' => ['required', 'max:50'],
      
            'email' => ['required', 'max:50', 'email','unique:users'],
            // 'password' => ['required','string','confirmed','min:8'],
            // 'mobile' => ['required','numeric'],
        ]);
        
        try{
            DB::beginTransaction();

            $newUser = new User();

            $newUser->name = $request->name;
    
            $newUser->email = $request->email;
            $newUser->password = Hash::make('Abcd@1234');
            $newUser->save();
            $newUser->assignRole($request->role_name);
            DB::commit();

            return redirect(route('user.index'));

        }catch(Exception $ex){
            DB::rollback();
            \Log::error($ex);
            return abort(500);
        }

    }

    public function update(Request $request)
    {
        // if (!auth()->user()->can('users.edit')) {
        //     abort(403, 'Unauthorized action.');
        // }

        $user_id = $request->id;

    //    $request->validate([
    //         'first_name' => ['required', 'max:50'],
    //         'last_name' => ['required', 'max:50'],
    //         'username'=> ['required', 'max:50', 'string','unique:users,name,'.$user_id],
    //         'email' => ['required', 'max:50', 'email','unique:users,email,'.$user_id],
    //         'mobile' => ['required','numeric'],
    //     ]);

        // if(isset($request->password)){
        //     $request->validate([
        //         'password' => ['required','string','confirmed','min:8'],
        //     ]);
        // }


        try{
            DB::beginTransaction();

            $User = User::find($user_id);

            // $User->first_name = $request->first_name;
            // $User->last_name = $request->last_name;
            $User->name = $request->name;
            $User->email = $request->email;
            // $User->role = $request->role;

            // if(isset($request->password)){
            //     $User->password = Hash::make($request->password);
            // }

            $User->save();


            DB::commit();

            return redirect(route('users.index'));

        }catch(Exception $ex){
            DB::rollback();
            \Log::error($ex);
            return abort(500);
        }

    }

}
