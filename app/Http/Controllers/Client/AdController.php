<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdController extends Controller
{
    public function index()
    {
        return view('client.my.ads.index');
    }

    public function create()
    {
        return view('client.my.ads.create');
    }
}
