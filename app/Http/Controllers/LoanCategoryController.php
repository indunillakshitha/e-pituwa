<?php

namespace App\Http\Controllers;

use App\Models\loanCategory;
use Illuminate\Http\Request;

class LoanCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\loanCategory  $loanCategory
     * @return \Illuminate\Http\Response
     */
    public function show(loanCategory $loanCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\loanCategory  $loanCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(loanCategory $loanCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\loanCategory  $loanCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, loanCategory $loanCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\loanCategory  $loanCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(loanCategory $loanCategory)
    {
        //
    }
}
