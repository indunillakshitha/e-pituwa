<?php

namespace App\Http\Controllers;

use App\Models\Utility\PermissionsUtility;
use Exception;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;
use DataTables;

class RoleController extends Controller
{
    public function getAllPermissions(){
        return Permission::select('id','section_name','name',
        DB::raw("CONCAT(UPPER(SUBSTRING(REPLACE(name, '.', ' '),1,1)),LOWER(SUBSTRING(REPLACE(name, '.', ' '),2))) as ft_name")
        )->get()->groupBy('section_name');
    }
    //--------------------------------RETURN TO ROLES INDEX VIEW---------------------
    public function index()
    {
    
        $roles = Role::with('permissions')->get();
        return Inertia::render('Admin/Role/Index', compact('roles'));
    }

    //--------------------------------RETURN TO ROLES CREATE VIEW---------------------
    public function create()
    {
        

         $all_permissions = $this->getAllPermissions();
        return Inertia::render('Admin/Role/Create', compact('all_permissions'));
       
    }

    //--------------------------------ROLES CREATE ---------------------
    public function store(Request $request)
    {
     
        try {
            DB::beginTransaction();

            Role::create(['guard_name'=>'web', 'name' => $request->role_name])->syncPermissions($request->permissions);
            DB::commit();
            // return 123;

            return redirect(route('roles.create'));

        } catch (Exception $e) {
            DB::rollBack();
            \Log::error($e);
            abort(500);
        }
    }


    //--------------------------------RETURN TO ROLES EDIT VIEW---------------------
    public function edit($id)
    {
        $edit_role = Role::with('permissions')->find($id);
        $all_permissions = $this->getAllPermissions();
        // $this_role_permissions = Role::find($id)->permissions;  #this works and comes with the pivot table
        // return $permissions;

        return Inertia::render('Admin/Role/CreateUpdate', compact('all_permissions', 'edit_role'));
    }

    //--------------------------------RETURN TO ROLES UPDATE VIEW---------------------
    public function update(Request $request)
    {
        try {
            // return $request;
            DB::beginTransaction();

            $role = Role::find($request->id);
            $role->syncPermissions($request->permissions);
            $role->name = $request->role_name;
            $role->save();

            DB::commit();

            return redirect(route('roles.index'));
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error($e);
            abort(500);
        }
    }

    public function chnage_role_status(Request $request)
    {
        $role = Role::find($request->id);
        $role->status = $request->status;
        $role->save();
        return response()->json($request);
    }

    //--------------------------------RETURN TO ROLES DELETE VIEW---------------------
    public function destroy(Request $request)
    {

        try{
            Role::find($request->id)->delete();
            return redirect(route('roles.index'));
        }catch(Exception $ex){
            return redirect(route('roles.index'))->with('status',500);
        }
    }



    public function getdata(Request $request){
        $roles = Role::with('permissions')->get();

        return DataTables::of($roles)
        ->addColumn('action', function($row){

            if($row->name == config('custom_config.super_admin_role')){
                return '';
            }

            $action_html = '';

            if(auth()->user()->can('roles.edit')){
                $action_html.='<a class="dropdown-item action_edit" data-user-id="'.$row->id.'" href="javascript:void(0)"><i class="fas fa-edit mr-2"></i> Edit</a>';
            }
            if(auth()->user()->can('roles.delete')){
                $action_html.='<a class="dropdown-item text-danger action_delete" data-user-id="'.$row->id.'" href="javascript:void(0)"><i class="fas fa-trash mr-2"></i> Delete</a> ';
            }

            return '<div class="btn-group">
                                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                '.$action_html.'
                                </div>
                            </div>';
        })
        ->rawColumns(['action'])
        ->make(true);
    }
}
