<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [


            //Users Section
            ['section' => 'users', 'name' => 'users.index'],
            ['name' => 'users.create', 'section' => 'users'],
            ['name' => 'users.store', 'section' => 'users'],
            ['name' => 'users.block', 'section' => 'users'],
            ['name' => 'users.update', 'section' => 'users'],
            ['name' => 'users.edit', 'section' => 'users'],

            //Roles Sectio
            ['name' => 'roles.index', 'section' => 'role'],
            ['name' => 'roles.create', 'section' => 'role'],
            ['name' => 'roles.store', 'section' => 'role'],
            ['name' => 'roles.delete', 'section' => 'role'],
            ['name' => 'roles.update', 'section' => 'role'],
            ['name' => 'roles.edit', 'section' => 'role'],


        ];



        //************ UNCOMMENT THESE SECTION ON FIRST SEED************//
        $user['name'] = 'Super admin';
        $user['email'] = 'suadmin@gmail.com';
        $user['password'] = Hash::make('Abcd@1234');

        $role = Role::where('name', 'Super Admin')->first();
        if (!isset($role)) {
            $role = Role::create(['name' => 'Super Admin']);
        }
        foreach ($permission_array as $permission) {

            $check_has_permission = Permission::where('name', $permission['name'])->first();
            if (!isset($check_has_permission)) {
                Permission::create($permission);
            }

            $role->givePermissionTo($permission['name']);
        }

        $user = User::where('email', 'suadmin@gmail.com')->first();
        if (!isset($user)) {
            $user = User::create([
                'name'=>'Super admin',
                'email'=>'suadmin@gmail.com',
                'password'=>Hash::make('Abcd@1234')
            ]);
        }
        $user->assignRole('Super Admin');
    }
}
