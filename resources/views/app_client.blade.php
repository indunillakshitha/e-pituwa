<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from amentotech.com/htmls/psello/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Feb 2020 13:19:37 GMT -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>E Pituwa</title>
    <link rel="apple-touch-icon" href="apple-touch-icon.png')}}">
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
    @include('client.layouts.header')
</head>

<body>

    @include('client.layouts.nav')
    @yield('content')

    @include('client.layouts.footer')

</body>

<!-- Mirrored from amentotech.com/htmls/psello/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Feb 2020 13:21:39 GMT -->

</html>
