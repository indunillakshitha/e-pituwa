@extends('app_client')

@section('content')


    <!-- BANNER START -->
    <div class="ps-main-banner">
        <div id="owl-first" class="owl-carousel owl-theme">
            <div class="item">
                <div class="ps-banner-img">
                    <figure><img src="{{ asset('images/banner.jpg') }}" alt="Image Description"></figure>
                </div>
            </div>
            <div class="item">
                <div class="ps-banner-img">
                    <figure><img src="{{ asset('images/banner2.jpg') }}" alt="Image Description"></figure>
                </div>
            </div>
            <div class="item">
                <div class="ps-banner-img">
                    <figure><img src="{{ asset('images/banner3.jpg') }}" alt="Image Description"></figure>
                </div>
            </div>
            <div class="item">
                <div class="ps-banner-img">
                    <figure><img src="{{ asset('images/banner4.jpg') }}" alt="Image Description"></figure>
                </div>
            </div>
            <div class="item">
                <div class="ps-banner-img">
                    <figure><img src="{{ asset('images/banner5.jpg') }}" alt="Image Description"></figure>
                </div>
            </div>
            <div class="item">
                <div class="ps-banner-img">
                    <figure><img src="{{ asset('images/banner6.jpg') }}" alt="Image Description"></figure>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-10">
                    <div class="ps-main-banner-content">
                        <div class="ps-main-banner-text animate" data-animate="fadeInLeft">
                            <h1><span>Search, Sell & Buy</span> That's <span>All We Do</span></h1>
                            <p>Consectetur adipisicing eliteiuim seteiusmod tempor incididunt utnae labore etnalom magna
                                aliqua udiminimate</p>
                        </div>
                        <form class="ps-form animate" data-animate="fadeInRight">
                            <div class="ps-form__input"><input class="form-control"
                                    placeholder="What Are You Looking For?"></div>
                            <div>
                                <div class="ps-geo-location ps-location">
                                    <input type="text" class="form-control" placeholder="Location*">
                                    <a href="javascript:void(0);" class="ps-location-icon ps-index-icon"><i
                                            class="ti-target"></i></a>
                                    <a href="javascript:void(0);" class="ps-arrow-icon ps-index-icon"><i
                                            class="ti-angle-down"></i></a>
                                    <div class="ps-distance">
                                        <div class="ps-distance__description">
                                            <label for="amountfour">Distance:</label>
                                            <input type="text" id="amountfour" readonly>
                                        </div>
                                        <div id="slider-range-min"></div>
                                    </div>
                                </div>
                                <button class="btn ps-btn">Search Now</button>
                            </div>
                        </form>
                        <div class="ps-banner-down-content">
                            <h6 class="animate" data-animate="fadeInTop">Start With Top Searched Categories:</h6>
                            <figure class="animate" data-animate="swing">
                                <img src="{{ asset('images/arrowcurve.png') }}" alt="Image Description">
                            </figure>
                            <div class="ps-banner--line animate" data-animate="fadeInBottom">
                                <div class="ps-banner__line"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="owl-one" class="ps-slider owl-carousel owl-theme">
            <div class="item">
                <div class="ps-slider--category">
                    <div class="ps-slider--circle">
                        <figure><img src="{{ asset('images/phone.png') }}" alt="Image Description"></figure>
                        <div></div>
                    </div>
                    <div class="ps-category__title">
                        <h6>Mobiles/Tablets</h6>
                        <p>35523 Items</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ps-slider--category">
                    <div class="ps-slider--circle">
                        <figure><img src="{{ asset('images/car.png') }}" alt="Image Description"></figure>
                        <div></div>
                    </div>
                    <div class="ps-category__title">
                        <h6>Vehicles</h6>
                        <p>122563 Items</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ps-slider--category">
                    <div class="ps-slider--circle">
                        <figure><img src="{{ asset('images/home.png') }}" alt="Image Description"></figure>
                        <div></div>
                    </div>
                    <div class="ps-category__title">
                        <h6>Property for Sale</h6>
                        <p>52257 Items</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ps-slider--category">
                    <div class="ps-slider--circle">
                        <figure><img src="{{ asset('images/rent.png') }}" alt="Image Description"></figure>
                        <div></div>
                    </div>
                    <div class="ps-category__title">
                        <h6>Property For Rent</h6>
                        <p>15523 Items</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ps-slider--category">
                    <div class="ps-slider--circle">
                        <figure><img src="{{ asset('images/oven.jpg') }}" alt="Image Description"></figure>
                        <div></div>
                    </div>
                    <div class="ps-category__title">
                        <h6>Home Electronics</h6>
                        <p>122563 Items</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ps-slider--category">
                    <div class="ps-slider--circle">
                        <figure><img src="{{ asset('images/bike.png') }}" alt="Image Description"></figure>
                        <div></div>
                    </div>
                    <div class="ps-category__title">
                        <h6>Bikes And Scooters</h6>
                        <p>4593 Items</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BANNER END -->
    <!-- MAIN START -->
    <main class="ps-main">
        <!-- INTRO START -->
        <section class="ps-main-section ps-intro">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-xl-6">
                        <div class="ps-intro__img animate" data-animate="fadeInLeft">
                            <figure>
                                <img src="{{ asset('images/buildings.png') }}" alt="Imgae Description">
                            </figure>
                        </div>
                    </div>
                    <div class="col-12 col-xl-6">
                        <div class="ps-intro__description animate" data-animate="fadeInRight">
                            <h2><span>Best Platform For</span> Buying & Selling</h2>
                            <p>Consectetur adipisicing eliteiuim lokat seteiusmod tempor incididunt utnae labore etnalom
                                magnate aliqua udiminimate losiame pistu.</p>
                            <div class="ps-intro__btn">
                                <button class="btn ps-btn ps-intro__btn--fill">Find Out More</button>
                                <button class="btn ps-btn ps-intro__btn--outline ps-outline-btn">Our Team</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- INTRO END -->
        <!-- HOW IT WORKS START -->
        <section class="ps-main-section ps-howitworks">
            <div class="container">
                <div class="ps-center-content">
                    <div class="row">
                        <div class="col-md-11 col-lg-8 col-xl-7">
                            <div class="ps-center-description">
                                <h6 class="animate" data-animate="swoopInLeft">We Made It Simple</h6>
                                <h4 class="animate" data-animate="swoopInRight">How It Works?</h4>
                                <p class="animate" data-animate="fadeIn">Consectetur adipisicing eliteiuim set
                                    eiusmod tempor incididunt labore etnalom dolore magna aliqua udiminimate veniam quistan
                                    norud.</p>
                            </div>
                        </div>
                    </div>
                    <div class="ps-howitworks__imges">
                        <div class="ps-howitworks__imges__content">
                            <div class="ps-howitworks__img">
                                <figure><img src="{{ asset('images/howit-works/box-down_arrow.png') }}"
                                        alt="Image Description"></figure>
                                <div class="ps-howitworks__dashcircle">
                                    <svg>
                                        <circle cx="50%" cy="50%" r="50%"></circle>
                                    </svg>
                                </div>
                            </div>
                            <div class="ps-howitworks__img__description">
                                <h5><span>Search Best Online</span> Products / Items</h5>
                            </div>
                        </div>
                        <div class="ps-howitworks__imges__content">
                            <div class="ps-howitworks__img">
                                <figure><img src="{{ asset('images/howit-works/person.png') }}" alt="Image Description">
                                </figure>
                                <div class="ps-howitworks__dashcircle">
                                    <svg>
                                        <circle cx="50%" cy="50%" r="50%"></circle>
                                    </svg>
                                </div>
                            </div>
                            <div class="ps-howitworks__img__description">
                                <h5><span>Contact Direct To</span> Seller User</h5>
                            </div>
                        </div>
                        <div class="ps-howitworks__imges__content">
                            <div class="ps-howitworks__img">
                                <figure><img src="{{ asset('images/howit-works/star.png') }}" alt="Image Description">
                                </figure>
                                <div class="ps-howitworks__dashcircle">
                                    <svg>
                                        <circle cx="50%" cy="50%" r="50%"></circle>
                                    </svg>
                                </div>
                            </div>
                            <div class="ps-howitworks__img__description">
                                <h5><span>Leave Your</span> Feedback</h5>
                            </div>
                        </div>
                        <div class="ps-howitworks__dashline">
                            <svg>
                                <line x1="0" y1="5" x2="100%" y2="5" stroke="black" stroke-dasharray="4 1" />
                            </svg>
                        </div>
                        <div class="ps-howitworks__arrow1">
                            <figure><img src="{{ asset('images/howit-works/arrow.png') }}" alt="Image Description">
                            </figure>
                        </div>
                        <div class="ps-howitworks__arrow2">
                            <figure><img src="{{ asset('images/howit-works/arrow.png') }}" alt="Image Description">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- HOW IT WORKS END -->
        <!-- FEATURED START -->
        <section class="ps-main-section ps-featured">
            <div class="container">
                <div class="ps-center-content">
                    <div class="ps-center-description ps-featured__description">
                        <h6 class="animate" data-animate="swoopInLeft">Start With Top Quality</h6>
                        <h4 class="animate" data-animate="swoopInRight">Featured Ads</h4>
                        <p class="animate" data-animate="fadeIn">Consectetur adipisicing eliteiuim set eiusmod
                            tempor incididunt labore etnalom dolore magna aliqua udiminimate veniam quistan norud.</p>
                    </div>
                    <div id="owl-two" class="ps-featured--cards owl-carousel owl-theme">
                        <div class="item">
                            <a href="{{route('client.listing.index')}}" >
                                <div class="card">
                                    <figure>
                                        <img src="{{ asset('images/featured/img-01.jpg') }}" class="card-img-top"
                                            alt="Image Description">
                                        <div></div>
                                    </figure>
                                    <span class="ps-tag">Featured</span>
                                    <span class="ps-tag--arrow"></span>
                                    <div class="card-body">
                                        <h6>$1,149</h6>
                                        <p class="card-text">Brand New Iphone X For Sale</p>
                                        <span class="d-block"><i class="ti-layers"></i> <a
                                                href="javascript:void(0);">Electronics</a></span>
                                        <span><i class="ti-time"></i> <span>Jun 27, 2019</span></span>
                                        <figure><img src="{{ asset('images/user-icon/img-01.jpg') }}"
                                                alt="Image Description"></figure>
                                    </div>
                                    <ul class="list-group">
                                        <li class="list-group-item"><span><i class="ti-map"></i> <a
                                                    href="javascript:void(0);">Manchester, UK</a></span><a
                                                href="javascript:void(0);"><i class="far fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <div class="card">
                                <figure>
                                    <img src="{{ asset('images/featured/img-02.jpg') }}" class="card-img-top"
                                        alt="Image Description">
                                    <div></div>
                                </figure>
                                <span class="ps-tag">Featured</span>
                                <span class="ps-tag--arrow"></span>
                                <div class="card-body">
                                    <h6>$650</h6>
                                    <p class="card-text">Galaxy Note 8 Urgent Sale</p>
                                    <span class="d-block"><i class="ti-layers"></i> <a
                                            href="javascript:void(0);">Electronics</a></span>
                                    <span><i class="ti-time"></i> <span>Jun 27, 2019</span></span>
                                    <figure><img src="{{ asset('images/user-icon/img-02.png') }}" alt="Image Description">
                                    </figure>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><span><i class="ti-map"></i> <a
                                                href="javascript:void(0);">Manchester, UK</a></span><a
                                            href="javascript:void(0);"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <figure>
                                    <img src="{{ asset('images/featured/img-03.jpg') }}" class="card-img-top"
                                        alt="Image Description">
                                    <div></div>
                                </figure>
                                <span class="ps-tag">Featured</span>
                                <span class="ps-tag--arrow"></span>
                                <div class="card-body">
                                    <h6>$1,200</h6>
                                    <p class="card-text">Mac Air Book Pro, Slightly Used</p>
                                    <span class="d-block"><i class="ti-layers"></i> <a
                                            href="javascript:void(0);">Electronics</a></span>
                                    <span><i class="ti-time"></i> <span>Jun 27, 2019</span></span>
                                    <figure><img src="{{ asset('images/user-icon/img-03.jpg') }}" alt="Image Description">
                                    </figure>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><span><i class="ti-map"></i> <a
                                                href="javascript:void(0);">Manchester, UK</a></span><a
                                            href="javascript:void(0);"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <figure>
                                    <img src="{{ asset('images/featured/img-04.jpg') }}" class="card-img-top"
                                        alt="Image Description">
                                    <div></div>
                                </figure>
                                <span class="ps-tag">Featured</span>
                                <span class="ps-tag--arrow"></span>
                                <div class="card-body">
                                    <h6>$1,149</h6>
                                    <p class="card-text">Brand New Touch Book For Sale</p>
                                    <span class="d-block"><i class="ti-layers"></i> <a
                                            href="javascript:void(0);">Electronics</a></span>
                                    <span><i class="ti-time"></i> <span>Jun 27, 2019</span></span>
                                    <figure><img src="{{ asset('images/user-icon/img-04.jpg') }}" alt="Image Description">
                                    </figure>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><span><i class="ti-map"></i> <a
                                                href="javascript:void(0);">Manchester, UK</a></span><a
                                            href="javascript:void(0);"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <figure>
                                    <img src="{{ asset('images/featured/img-01.jpg') }}" class="card-img-top"
                                        alt="Image Description">
                                    <div></div>
                                </figure>
                                <span class="ps-tag">Featured</span>
                                <span class="ps-tag--arrow"></span>
                                <div class="card-body">
                                    <h6>$1,149</h6>
                                    <p class="card-text">Brand New Iphone X For Sale</p>
                                    <span class="d-block"><i class="ti-layers"></i> <a
                                            href="javascript:void(0);">Electronics</a></span>
                                    <span><i class="ti-time"></i> <span>Jun 27, 2019</span></span>
                                    <figure><img src="{{ asset('images/user-icon/img-01.jpg') }}" alt="Image Description">
                                    </figure>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><span><i class="ti-map"></i> <a
                                                href="javascript:void(0);">Manchester, UK</a></span><a
                                            href="javascript:void(0);"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <figure>
                                    <img src="{{ asset('images/featured/img-02.jpg') }}" class="card-img-top"
                                        alt="Image Description">
                                    <div></div>
                                </figure>
                                <span class="ps-tag">Featured</span>
                                <span class="ps-tag--arrow"></span>
                                <div class="card-body">
                                    <h6>$650</h6>
                                    <p class="card-text">Galaxy Note 8 Urgent Sale</p>
                                    <span class="d-block"><i class="ti-layers"></i> <a
                                            href="javascript:void(0);">Electronics</a></span>
                                    <span><i class="ti-time"></i> <span>Jun 27, 2019</span></span>
                                    <figure><img src="{{ asset('images/user-icon/img-02.png') }}" alt="Image Description">
                                    </figure>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><span><i class="ti-map"></i> <a
                                                href="javascript:void(0);">Manchester, UK</a></span><a
                                            href="javascript:void(0);"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <figure>
                                    <img src="{{ asset('images/featured/img-03.jpg') }}" class="card-img-top"
                                        alt="Image Description">
                                    <div></div>
                                </figure>
                                <span class="ps-tag">Featured</span>
                                <span class="ps-tag--arrow"></span>
                                <div class="card-body">
                                    <h6>$1,200</h6>
                                    <p class="card-text">Mac Air Book Pro, Slightly Used</p>
                                    <span class="d-block"><i class="ti-layers"></i> <a
                                            href="javascript:void(0);">Electronics</a></span>
                                    <span><i class="ti-time"></i> <span>Jun 27, 2019</span></span>
                                    <figure><img src="{{ asset('images/user-icon/img-03.jpg') }}" alt="Image Description">
                                    </figure>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><span><i class="ti-map"></i> <a
                                                href="javascript:void(0);">Manchester, UK</a></span><a
                                            href="javascript:void(0);"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <figure>
                                    <img src="{{ asset('images/featured/img-04.jpg') }}" class="card-img-top"
                                        alt="Image Description">
                                    <div></div>
                                </figure>
                                <span class="ps-tag">Featured</span>
                                <span class="ps-tag--arrow"></span>
                                <div class="card-body">
                                    <h6>$1,149</h6>
                                    <p class="card-text">Brand New Touch Book For Sale</p>
                                    <span class="d-block"><i class="ti-layers"></i> <a
                                            href="javascript:void(0);">Electronics</a></span>
                                    <span><i class="ti-time"></i> <span>Jun 27, 2019</span></span>
                                    <figure><img src="{{ asset('images/user-icon/img-04.jpg') }}" alt="Image Description">
                                    </figure>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><span><i class="ti-map"></i> <a
                                                href="javascript:void(0);">Manchester, UK</a></span><a
                                            href="javascript:void(0);"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- FEATURED END -->
        <!-- STATUS START -->
        <section class="ps-main-section ps-status">
            <div class="container">
                <div class="ps-center-content">
                    <div class="ps-center-description">
                        <h6 class="animate" data-animate="swoopInLeft">Why We’re Best</h6>
                        <h4 class="animate" data-animate="swoopInRight">Stat Says It All</h4>
                        <p class="animate" data-animate="fadeIn">Consectetur adipisicing eliteiuim set eiusmod
                            tempor incididunt labore etnalom dolore magna aliqua udiminimate veniam quistan norud.</p>
                    </div>
                    <div class="ps-status__content" id="counter">
                        <div class="ps-status__item">
                            <div class="ps-status__img">
                                <figure><img src="{{ asset('images/ps-status/img-01.png') }}" alt="Image Description">
                                </figure>
                            </div>
                            <div class="ps-status__title">
                                <h5><span class="count" data-count="35">0</span>,<span class="count"
                                        data-count="6820"></span></h5>
                                <h6>Active Items</h6>
                            </div>
                        </div>
                        <div class="ps-status__item">
                            <div class="ps-status__img">
                                <figure><img src="{{ asset('images/ps-status/img-02.png') }}" alt="Image Description">
                                </figure>
                            </div>
                            <div class="ps-status__title">
                                <h5><span class="count" data-count="99.56%">0</span>%</h5>
                                <h6>User Feedback</h6>
                            </div>
                        </div>
                        <div class="ps-status__item">
                            <div class="ps-status__img">
                                <figure><img src="{{ asset('images/ps-status/img-03.png') }}" alt="Image Description">
                                </figure>
                            </div>
                            <div class="ps-status__title">
                                <h5><span class="count" data-count="45">0</span>,<span class="count"
                                        data-count="824"></span>,<span class="count" data-count="89"></span></h5>
                                <h6>Items Sold</h6>
                            </div>
                        </div>
                        <div class="ps-status__item">
                            <div class="ps-status__img">
                                <figure><img src="{{ asset('images/ps-status/img-04.png') }}" alt="Image Description">
                                </figure>
                            </div>
                            <div class="ps-status__title">
                                <h5><span class="count" data-count="12">0</span>,<span class="count"
                                        data-count="0"></span><span class="count" data-count="68"></span></h5>
                                <h6>Cup Of Coffee</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- STATUS END -->
        <!-- VIDEO START -->
        <section class="ps-video">
            <a class="ps-video__img" data-rel="prettyPhoto" href="https://youtu.be/XxxIEGzhIG8">
                <figure><img src="{{ asset('images/ps-video/icon/img-01.png') }}" alt="Image Description"></figure>
            </a>
        </section>
        <!-- VIDEO END -->
        <!-- CATEGORIES START -->
        <section class="ps-main-section ps-common-categories">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 col-lg-8 col-xl-7">
                        <div class="ps-center-content">
                            <div class="ps-center-description">
                                <h6 class="animate" data-animate="swoopInLeft">Start Exploring With Our</h6>
                                <h4 class="animate" data-animate="swoopInRight">Common Categories</h4>
                                <p class="animate" data-animate="fadeIn">Consectetur adipisicing eliteiuim set
                                    eiusmod tempor incididunt labore etnalom dolore magna aliqua udiminimate veniam quistan
                                    norud.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-lg-4 col-xl-3">
                        <div class="ps-common-categories__content">
                            <div class="ps-common-categories__img">
                                <figure><img src="{{ asset('images/common-categories/img-01.png') }}"
                                        alt="Image Description"></figure>
                                <div></div>
                            </div>
                            <div class="ps-common-categories__title">
                                <h6>Mobiles/Tablets</h6>
                                <p>35523 Items</p>
                            </div>
                            <div class="ps-common-categories__hoverbtn">
                                <button class="btn">Show All</button>
                            </div>
                            <div class="ps-common-categories__hoverimg">
                                <figure><img src="{{ asset('images/common-categories/hover/img-01.png') }}"
                                        alt="Image Description"></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-4 col-xl-3">
                        <div class="ps-common-categories__content">
                            <div class="ps-common-categories__img">
                                <figure><img src="{{ asset('images/common-categories/img-02.png') }}"
                                        alt="Image Description"></figure>
                                <div></div>
                            </div>
                            <div class="ps-common-categories__title">
                                <h6>Vehicles</h6>
                                <p>122563 Items</p>
                            </div>
                            <div class="ps-common-categories__hoverbtn">
                                <button class="btn">Show All</button>
                            </div>
                            <div class="ps-common-categories__hoverimg">
                                <figure><img src="{{ asset('images/common-categories/hover/img-02.png') }}"
                                        alt="Image Description"></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-4 col-xl-3">
                        <div class="ps-common-categories__content">
                            <div class="ps-common-categories__img">
                                <figure><img src="{{ asset('images/common-categories/img-03.png') }}"
                                        alt="Image Description"></figure>
                                <div></div>
                            </div>
                            <div class="ps-common-categories__title">
                                <h6>Property for Sale</h6>
                                <p>52257 Items</p>
                            </div>
                            <div class="ps-common-categories__hoverbtn">
                                <button class="btn">Show All</button>
                            </div>
                            <div class="ps-common-categories__hoverimg">
                                <figure><img src="{{ asset('images/common-categories/hover/img-03.png') }}"
                                        alt="Image Description"></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-4 col-xl-3">
                        <div class="ps-common-categories__content">
                            <div class="ps-common-categories__img">
                                <figure><img src="{{ asset('images/common-categories/img-04.png') }}"
                                        alt="Image Description"></figure>
                                <div></div>
                            </div>
                            <div class="ps-common-categories__title">
                                <h6>Property For Rent</h6>
                                <p>15523 Items</p>
                            </div>
                            <div class="ps-common-categories__hoverbtn">
                                <button class="btn">Show All</button>
                            </div>
                            <div class="ps-common-categories__hoverimg">
                                <figure><img src="{{ asset('images/common-categories/hover/img-04.png') }}"
                                        alt="Image Description"></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-4 col-xl-3">
                        <div class="ps-common-categories__content">
                            <div class="ps-common-categories__img">
                                <figure><img src="{{ asset('images/common-categories/img-05.png') }}"
                                        alt="Image Description"></figure>
                                <div></div>
                            </div>
                            <div class="ps-common-categories__title">
                                <h6>Home Electronics</h6>
                                <p>122563 Items</p>
                            </div>
                            <div class="ps-common-categories__hoverbtn">
                                <button class="btn">Show All</button>
                            </div>
                            <div class="ps-common-categories__hoverimg">
                                <figure><img src="{{ asset('images/common-categories/hover/img-05.png') }}"
                                        alt="Image Description"></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-4 col-xl-3">
                        <div class="ps-common-categories__content">
                            <div class="ps-common-categories__img">
                                <figure><img src="{{ asset('images/common-categories/img-06.png') }}"
                                        alt="Image Description"></figure>
                                <div></div>
                            </div>
                            <div class="ps-common-categories__title">
                                <h6>Bikes And Scooters</h6>
                                <p>4593 Items</p>
                            </div>
                            <div class="ps-common-categories__hoverbtn">
                                <button class="btn">Show All</button>
                            </div>
                            <div class="ps-common-categories__hoverimg">
                                <figure><img src="{{ asset('images/common-categories/hover/img-06.png') }}"
                                        alt="Image Description"></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-4 col-xl-3">
                        <div class="ps-common-categories__content">
                            <div class="ps-common-categories__img">
                                <figure><img src="{{ asset('images/common-categories/img-07.png') }}"
                                        alt="Image Description"></figure>
                                <div></div>
                            </div>
                            <div class="ps-common-categories__title">
                                <h6>Business & Industrial</h6>
                                <p>14525 Items</p>
                            </div>
                            <div class="ps-common-categories__hoverbtn">
                                <button class="btn">Show All</button>
                            </div>
                            <div class="ps-common-categories__hoverimg">
                                <figure><img src="{{ asset('images/common-categories/hover/img-07.png') }}"
                                        alt="Image Description"></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-4 col-xl-3">
                        <div class="ps-common-categories__content">
                            <div class="ps-common-categories__img">
                                <figure><img src="{{ asset('images/common-categories/img-08.png') }}"
                                        alt="Image Description"></figure>
                                <div></div>
                            </div>
                            <div class="ps-common-categories__title">
                                <h6>Services</h6>
                                <p>52342 Items</p>
                            </div>
                            <div class="ps-common-categories__hoverbtn">
                                <button class="btn">Show All</button>
                            </div>
                            <div class="ps-common-categories__hoverimg">
                                <figure><img src="{{ asset('images/common-categories/hover/img-08.png') }}"
                                        alt="Image Description"></figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- CATEGORIES END -->
        <!-- MOBILE START -->
        <section class="ps-mobile">
            <div class="ps-dark-overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-9 col-xl-8">
                            <div class="ps-main-section ps-center-content">
                                <div class="ps-center-description">
                                    <h3 class="animate" data-animate="swoopInLeft">Introducing All New</h3>
                                    <h2 class="animate" data-animate="swoopInRight">Our Native Mobile App</h2>
                                    <div class="animate" data-animate="fadeIn">
                                        <p>Consectetur adipisicing eliteiuim lokat seteiusmod tempor incididunt utnae labore
                                            etnalom magnate aliqua udiminimate losiame pistu.</p>
                                    </div>
                                </div>
                                <div class="ps-mobile__button">
                                    <a href="javascript:void(0);">
                                        <figure><img src="{{ asset('images/native-mobile/img-03.png') }}"
                                                alt="Image Description"></figure>
                                    </a>
                                    <a href="javascript:void(0);">
                                        <figure><img src="{{ asset('images/native-mobile/img-04.png') }}"
                                                alt="Image Description"></figure>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="ps-mobile__img">
                            <figure><img src="{{ asset('images/native-mobile/img-02.png') }}" alt="Image Description">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- MOBILE END -->
        <!-- ARTICLES START -->
        <section class="ps-main-section ps-articles">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 col-lg-8 col-xl-7">
                        <div class="ps-center-content">
                            <div class="ps-center-description">
                                <h6 class="animate" data-animate="swoopInLeft">Latest & Updated</h6>
                                <h4 class="animate" data-animate="swoopInRight">News Articles</h4>
                                <div class="animate" data-animate="fadeIn">
                                    <p>Consectetur adipisicing eliteiuim set eiusmod tempor incididunt labore etnalom dolore
                                        magna aliqua udiminimate veniam quistan norud.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="card">
                            <figure>
                                <img src="{{ asset('images/articles/img-01.jpg') }}" class="card-img-top"
                                    alt="Image Description">
                            </figure>
                            <div class="card-body">
                                <a href="javascript:void(0);">By Admin</a>
                                <h6 class="card-text">Make Money Easy As Pie</h6>
                                <span>Jun 27,2 019</span>
                                <p>Consectetur adipisicing elite aeiuim setiusm tempor incididunt labore etnalom...</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="card">
                            <figure>
                                <img src="{{ asset('images/articles/img-02.jpg') }}" class="card-img-top"
                                    alt="Image Description">
                            </figure>
                            <div class="card-body">
                                <a href="javascript:void(0);">By Almana Kurame</a>
                                <h6 class="card-text">Learn Top 10 Tricks To Sell Your Items</h6>
                                <span>Jun 27,2 019</span>
                                <p>Consectetur adipisicing elite aeiuim setiusm tempor incididunt labore etnalom...</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="card">
                            <figure>
                                <img src="{{ asset('images/articles/img-03.jpg') }}" class="card-img-top"
                                    alt="Image Description">
                            </figure>
                            <div class="card-body">
                                <a href="javascript:void(0);">By Ibrahim Styne</a>
                                <h6 class="card-text">Things You Should Start Doing Wi...</h6>
                                <span>Jun 27,2 019</span>
                                <p>Consectetur adipisicing elite aeiuim setiusm tempor incididunt labore etnalom Consectetur
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ARTICLES END -->
    </main>
    <!-- MAIN END -->


@endsection
