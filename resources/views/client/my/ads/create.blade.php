<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from amentotech.com/htmls/psello/dashboard-post-ad.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Feb 2020 13:24:59 GMT -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BootStrap HTML5 CSS3 Theme</title>
    <link rel="apple-touch-icon" href="apple-touch-icon.png')}}">
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">

    @include('client.my.layouts.header')
</head>

<body>
    <!-- PRELOADER START -->
    <div class="preloader-outer">
        <figure><img src="{{ asset('images/img-face.png') }}" alt="Image Description">
            <span class="pulse"></span>
        </figure>
    </div>
    <!-- PRELOADER END -->
    @include('client.layouts.nav')
    <!-- BANNER START -->
    <div class="ps-main-banner">
        <div class="ps-dark-overlay">
            <div class="container">
                <div class="ps-banner-content">
                    <h4>Post New Ad</h4>
                    <p><a href="index.html">Home</a> <span><i class="ti-angle-right"></i></span> <a
                            href="insights.html">Dashboard</a> <span> <i class="ti-angle-right"></i></span> Post Ad</p>
                </div>
            </div>
        </div>
    </div>
    <!-- BANNER END -->
    <!-- MAIN START -->
    <main class="ps-main">
        <section class="ps-main-section">
            <div class="container">
                <div class="row">
                    <!-- SIDEBAR START -->
                    <div class="col-lg-4">
                        <div class="ps-dashboard-sidebar">
                            <div class="ps-dashboard-sidebar__user">
                                <figure><img src="{{ asset('images/insights/user-img.jpg') }}" alt="Image Description">
                                </figure>
                                <div class="ps-seller__description">
                                    <h6>Lorina Statham</h6>
                                    <div class="ps-h5">Status:
                                        <a data-toggle="collapse" href="#collapseUser" role="button"
                                            aria-expanded="false" aria-controls="collapseUser">
                                            <span><em class="ps-online"></em><i
                                                    class="fa fa-sort-down"></i></span><em>Online</em>
                                        </a>
                                        <div class="collapse" id="collapseUser">
                                            <a href="javascript:void(0);"><span
                                                    class="ps-online"></span><em>Online</em></a>
                                            <a href="javascript:void(0);"><span
                                                    class="ps-away"></span><em>Away</em></a>
                                            <a href="javascript:void(0);"><span
                                                    class="ps-busy"></span><em>Busy</em></a>
                                            <a href="javascript:void(0);"><span
                                                    class="ps-offline"></span><em>Offline</em></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="ps-dashboard-sidebar__edit"><a href="javascript:void(0);"><i
                                            class="ti-pencil"></i></a></div>
                            </div>
                            <ul>
                                <li><a href="dashboard-insights.html"><i class="ti-dashboard"></i>
                                        <span>Insights</span> </a></li>
                                <li><a href="dashboard-profile-setting.html"><i class="ti-user"></i>
                                        <span>Profile Settings</span> </a></li>
                                <li><a href="dashboard-my-ads.html"><i class="ti-align-justify"></i> <span>My
                                            Ads</span> </a></li>
                                <li><a href="dashboard-post-ad.html" class="ps-user-dhb"><i
                                            class="ti-settings"></i> <span>Post Ad</span> </a></li>
                                <li><a href="dashboard-offers-messages.html"><i class="ti-email"></i>
                                        <span>Offers/messages</span> </a></li>
                                <li><a href="dashboard-buy-package.html" class="ps-active--line"><i
                                            class="ti-shopping-cart"></i> <span>Buy New Packages</span> </a></li>
                                <li><a href="dashboard-payments.html"><i class="ti-user"></i>
                                        <span>Payments</span> </a></li>
                                <li>
                                    <a data-toggle="collapse" href="#collapsenew" role="button" aria-expanded="false"><i
                                            class="ti-heart"></i> <span>My Favorite</span> <span
                                            class="ps-right"><i class="ti-angle-right"></i></span> </a>
                                    <ul class="collapse" id="collapsenew">
                                        <li><a href="dashboard-my-favorite.html"><span>Favorite Listing</span></a></li>
                                        <li><a href="javascript:void(0);"><span>Sub Menu</span></a></li>
                                    </ul>
                                </li>
                                <li><a href="dashboard-account-setting.html"><i class="ti-bookmark"></i>
                                        <span>Account & Privacy Settings</span> </a></li>
                                <li><a href="index.html"><i class="ti-shift-right"></i> <span>Logout</span> </a></li>
                            </ul>
                        </div>
                        <div class="ps-gridList__ad ps-dashboard-img">
                            <a href="javascript:void(0);">
                                <figure><img src="{{ asset('images/ad-img.jpg') }}" alt="Image Description"></figure>
                            </a>
                            <span>Advertisement 255px X 255px</span>
                        </div>
                    </div>
                    <!-- SIDEBAR END -->
                    <div class="col-lg-8 ps-dashboard-user">
                        <div class="ps-posted-ads ps-profile-setting">
                            <div class="ps-posted-ads__heading">
                                <h5>Post New Ad</h5>
                                <p>Click “Publish Ad” to Post Ad</p>
                                <button class="btn ps-btn">Publish Ad</button>
                            </div>
                            <div class="ps-profile-setting__content">
                                <!-- POST NEW AD FORM START -->
                                <form class="ps-profile-form">
                                    <div class="ps-profile--row">
                                        <div class="form-group">
                                            <label>
                                                <select class="form-control">
                                                    <option value="" disabled selected hidden>Featured Ad:</option>
                                                    <option value="">Yes</option>
                                                    <option value="">No</option>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <select class="form-control">
                                                    <option value="" disabled selected hidden>Select Category(s)*
                                                    </option>
                                                    <option value="">Yes</option>
                                                    <option value="">No</option>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="form-group ps-fullwidth">
                                            <input type="text" class="form-control" id="formGroupExampleInput"
                                                required placeholder="Your Ad Title Here*">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="formGroupExampleInput2"
                                                required placeholder="Item/ Product Price?*">
                                        </div>
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="formGroupExampleInput3"
                                                required placeholder="Mobile Number*">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="formGroupExampleInput4"
                                                placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="formGroupExampleInput5"
                                                placeholder="Skype">
                                        </div>
                                        <div class="form-group ps-fullwidth">
                                            <textarea class="form-control" placeholder="Description*"></textarea>
                                        </div>
                                    </div>
                                </form>
                                <!-- POST NEW AD FORM END -->
                                <!-- ADD FEATURES START -->
                                <div class="ps-add-feature">
                                    <div class="ps-add-feature__heading">
                                        <h5>Add Features</h5>
                                        <a data-toggle="collapse" href="#collapseFeature" role="button"
                                            aria-expanded="false" aria-controls="collapseFeature"><i
                                                class="ti-angle-down"></i></a>
                                    </div>
                                    <div class="collapse show" id="collapseFeature">
                                        <ul
                                            class="ps-profile-setting__imgs ps-add-feature__content ps-item-mesonry row ">
                                            <li class="ps-feature-select col-md-6 ps-ms-item">
                                                <h5>Printable:</h5>
                                                <input id="feature1" type="radio" name="print">
                                                <label for="feature1">
                                                    <span> No</span>
                                                </label>
                                                <input id="feature2" type="radio" name="print">
                                                <label for="feature2">
                                                    <span>Yes</span>
                                                </label>
                                            </li>
                                            <li class="ps-feature-select col-md-6 ps-ms-item">
                                                <h5>Paper Quality:</h5>
                                                <input id="feature3" type="radio" name="paper">
                                                <label for="feature3">
                                                    <span> Premium Quality</span>
                                                </label>
                                                <input id="feature4" type="radio" name="paper">
                                                <label for="feature4">
                                                    <span>Best Quality</span>
                                                </label>
                                                <input id="feature5" type="radio" name="paper">
                                                <label for="feature5">
                                                    <span>Regular Quality</span>
                                                </label>
                                            </li>
                                            <li class="ps-feature-select ps-paper-color col-md-6 ps-ms-item">
                                                <h5>Paper Color:</h5>
                                                <input id="feature6" type="radio" name="color">
                                                <label for="feature6">
                                                    <span class="ps-black">Black</span>
                                                </label>
                                                <input id="feature7" type="radio" name="color">
                                                <label for="feature7">
                                                    <span class="ps-off-white">Off White</span>
                                                </label>
                                                <input id="feature8" type="radio" name="color">
                                                <label for="feature8">
                                                    <span class="ps-yellow">Yellow</span>
                                                </label>
                                                <input id="feature9" type="radio" name="color">
                                                <label for="feature9">
                                                    <span class="ps-orange">Orange</span>
                                                </label>
                                                <input id="feature10" type="radio" name="color">
                                                <label for="feature10">
                                                    <span class="ps-blue">Blue</span>
                                                </label>
                                                <input id="feature11" type="radio" name="color">
                                                <label for="feature11">
                                                    <span class="ps-pink">Pink</span>
                                                </label>
                                                <input id="feature12" type="radio" name="color">
                                                <label for="feature12">
                                                    <span>Other</span>
                                                </label>
                                                <input type="text" class="form-control" placeholder="Add Color Name">
                                            </li>
                                            <li class="ps-feature-select col-md-6 ps-ms-item">
                                                <h5>Soft Copy:</h5>
                                                <input id="feature13" type="radio" name="softCopy">
                                                <label for="feature13">
                                                    <span>Availability On Demand</span>
                                                </label>
                                                <input id="feature14" type="radio" name="softCopy">
                                                <label for="feature14">
                                                    <span>Yes</span>
                                                </label>
                                                <input id="feature15" type="radio" name="softCopy">
                                                <label for="feature15">
                                                    <span>No</span>
                                                </label>
                                            </li>
                                            <li class="ps-feature-select col-md-6 ps-ms-item">
                                                <h5>Color/ B&W:</h5>
                                                <input id="feature16" type="radio" name="color">
                                                <label for="feature16">
                                                    <span>B&W Single Side</span>
                                                </label>
                                                <input id="feature17" type="radio" name="color">
                                                <label for="feature17">
                                                    <span>B&W Double Side</span>
                                                </label>
                                                <input id="feature18" type="radio" name="color">
                                                <label for="feature18">
                                                    <span>Color Single Side</span>
                                                </label>
                                                <input id="feature19" type="radio" name="color">
                                                <label for="feature19">
                                                    <span>Color Double Side</span>
                                                </label>
                                            </li>
                                            <li class="ps-feature-select col-md-6 ps-ms-item">
                                                <h5>Spring Bind:</h5>
                                                <input id="feature20" type="radio" name="bind">
                                                <label for="feature20">
                                                    <span>No</span>
                                                </label>
                                                <input id="feature21" type="radio" name="bind">
                                                <label for="feature21">
                                                    <span>Yes</span>
                                                </label>
                                            </li>
                                            <li class="ps-feature-select col-md-6 ps-ms-item">
                                                <h5>Door Step Delivery:</h5>
                                                <input id="feature22" type="radio" name="delivery">
                                                <label for="feature22">
                                                    <span>No</span>
                                                </label>
                                                <input id="feature23" type="radio" name="delivery">
                                                <label for="feature23">
                                                    <span>Yes</span>
                                                </label>
                                            </li>
                                            <li class="ps-feature-select col-md-6 ps-ms-item">
                                                <h5>Leminated:</h5>
                                                <input id="feature24" type="radio" name="leminate">
                                                <label for="feature24">
                                                    <span>No</span>
                                                </label>
                                                <input id="feature25" type="radio" name="leminate">
                                                <label for="feature25">
                                                    <span>Yes</span>
                                                </label>
                                            </li>
                                            <li class="ps-feature-select col-md-6 ps-ms-item">
                                                <h5>Color:</h5>
                                                <input id="feature26" type="checkbox" name="chooseColor">
                                                <label for="feature26">
                                                    <span>RGB</span>
                                                </label>
                                                <input id="feature27" type="checkbox" name="chooseColor">
                                                <label for="feature27">
                                                    <span>CMYK</span>
                                                </label>
                                                <input id="feature28" type="checkbox" name="chooseColor">
                                                <label for="feature28">
                                                    <span>B&W</span>
                                                </label>
                                                <input id="feature29" type="checkbox" name="chooseColor">
                                                <label for="feature29">
                                                    <span>Others</span>
                                                </label>
                                                <input type="text" class="form-control" placeholder="Mention Here">
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- ADD FEATURES END -->
                                <div class="ps-url">
                                    <div class="ps-url__input">
                                        <input type="text" class="form-control" placeholder="Ad Video Link(URL)">
                                        <button class="btn ps-blue ps-btn"><i class="ti-plus"></i></button>
                                    </div>
                                    <div class="ps-url__input">
                                        <input type="text" class="form-control" placeholder="Ad Video Link(URL)">
                                        <button class="btn ps-red ps-btn"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                                <!-- PROFILE SETTINGS START -->
                                <div class="ps-profile-setting__upload">
                                    <h5>Profile Settings</h5>
                                    <div class="ps-profile-setting__uploadarea">
                                        <button class="btn ps-btn">Select File</button>
                                        <p class="ps-drop">Drop files here to upload</p>
                                        <div></div>
                                        <p class="ps-loading">Uploading</p>
                                        <svg>
                                            <rect height="60px" width="100%" rx="6px" stroke-width="2"
                                                stroke-dasharray="12 12" />
                                        </svg>
                                    </div>
                                    <form class="ps-profile-setting__imgs ps-x-axis mCustomScrollbar">
                                        <label>
                                            <span>
                                                <img src="{{ asset('images/post-ad/img-01.jpg') }}"
                                                    alt="Image Description">
                                                <a href="javascript:void(0);" class="ps-trash"><span><i
                                                            class="ti-trash"></i></span></a>
                                                <span class="ps-tick"><span><i
                                                            class="fas fa-check"></i></span></span>
                                            </span>
                                        </label>
                                        <label>
                                            <span>
                                                <img src="{{ asset('images/post-ad/img-02.jpg') }}"
                                                    alt="Image Description">
                                                <a href="javascript:void(0);" class="ps-trash"><span><i
                                                            class="ti-trash"></i></span></a>
                                                <span class="ps-tick"><span><i
                                                            class="fas fa-check"></i></span></span>
                                            </span>
                                        </label>
                                        <label>
                                            <span>
                                                <img src="{{ asset('images/post-ad/img-03.jpg') }}"
                                                    alt="Image Description">
                                                <a href="javascript:void(0);" class="ps-trash"><span><i
                                                            class="ti-trash"></i></span></a>
                                                <span class="ps-tick"><span><i
                                                            class="fas fa-check"></i></span></span>
                                            </span>
                                        </label>
                                        <label>
                                            <span>
                                                <img src="{{ asset('images/post-ad/img-04.jpg') }}"
                                                    alt="Image Description">
                                                <a href="javascript:void(0);" class="ps-trash"><span><i
                                                            class="ti-trash"></i></span></a>
                                                <span class="ps-tick"><span><i
                                                            class="fas fa-check"></i></span></span>
                                            </span>
                                        </label>
                                        <label>
                                            <span>
                                                <img src="{{ asset('images/post-ad/img-05.jpg') }}"
                                                    alt="Image Description">
                                                <a href="javascript:void(0);" class="ps-trash"><span><i
                                                            class="ti-trash"></i></span></a>
                                                <span class="ps-tick"><span><i
                                                            class="fas fa-check"></i></span></span>
                                            </span>
                                        </label>
                                        <label>
                                            <span>
                                                <img src="{{ asset('images/post-ad/img-05.jpg') }}"
                                                    alt="Image Description">
                                                <a href="javascript:void(0);" class="ps-trash"><span><i
                                                            class="ti-trash"></i></span></a>
                                                <span class="ps-tick"><span><i
                                                            class="fas fa-check"></i></span></span>
                                            </span>
                                        </label>
                                    </form>
                                </div>
                                <!-- PROFILE SETTINGS END -->
                                <div class="ps-profile-setting__save">
                                    <button class="btn ps-btn">Save Changes</button>
                                    <p>Click “Save Changes” to update</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- MAIN END -->
    <!-- FOOTER START -->
    <footer class="ps-main-footer">
        <section class="ps-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-lg-6">
                        <div class="ps-footer__left">
                            <figure><img src="{{ asset('images/footer/img-01.png') }}" alt="Image Description">
                            </figure>
                            <p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt utbor etian dolm magna
                                aliqua enim ad minim veniam quis nostrud exercita ullamco laboris nisie aliquip commodo
                                okialama sikune pisuye.</p>
                            <ul class="ps-footer__contact">
                                <li><a href="javascript:void(0);"><i class="lnr lnr-location"></i> 123 New Design
                                        Street, Melbour Australia 54214</a></li>
                                <li><a href="javascript:void(0);"><i class="lnr lnr-envelope"></i>
                                        info@domainurl.com</a></li>
                                <li><a href="javascript:void(0);"><i class="lnr lnr-phone-handset"></i> (0800) 1234
                                        567891</a></li>
                            </ul>
                            <ul class="ps-footer__social-media">
                                <li class="ps-facebook"><a href="javascript:void(0);"><i
                                            class="fab fa-facebook-f"></i></a></li>
                                <li class="ps-twitter"><a href="javascript:void(0);"><i
                                            class="fab fa-twitter"></i></a></li>
                                <li class="ps-linkedin"><a href="javascript:void(0);"><i
                                            class="fab fa-linkedin-in"></i></a></li>
                                <li class="ps-instagram"><a href="javascript:void(0);"><i
                                            class="fab fa-instagram"></i></a></li>
                                <li class="ps-youtube"><a href="javascript:void(0);"><i
                                            class="fab fa-youtube"></i></a></li>
                                <li class="ps-google-plus"><a href="javascript:void(0);"><i
                                            class="fab fa-google-plus-g"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ps-footer__link">
                            <h6>Useful Links</h6>
                            <ul>
                                <li><a href="javascript:void(0);">How it works?</a></li>
                                <li><a href="javascript:void(0);">About</a></li>
                                <li><a href="javascript:void(0);">Listing Grid</a></li>
                                <li><a href="javascript:void(0);">Listing Single - With Chatbox</a></li>
                                <li><a href="javascript:void(0);">Latest Article Grid</a></li>
                                <li><a href="javascript:void(0);">Latest Article Detail</a></li>
                                <li><a href="javascript:void(0);">Login Or Register</a></li>
                                <li><a href="javascript:void(0);">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ps-footer__newsletter">
                            <h6>Newsletter</h6>
                            <p>Eiusmod tempor incididunt utbor etian dolm magna aliqua enim minim</p>
                            <div class="input-group">
                                <input type="email" class="form-control" placeholder="Your Email"
                                    aria-label="Your Email" aria-describedby="button-addon1">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon1"><i
                                            class="ti-arrow-right"></i></button>
                                </div>
                            </div>
                            <h6>Download Mobile App</h6>
                            <div class="ps-footer__img">
                                <a href="javascript:void(0);">
                                    <figure><img src="{{ asset('images/footer/img-02.png') }}" alt="Image Description">
                                    </figure>
                                </a>
                                <a href="javascript:void(0);">
                                    <figure><img src="{{ asset('images/footer/img-03.png') }}" alt="Image Description">
                                    </figure>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="ps-footer-down">
            <P>Copyrights © 2019 by<a href="javascript:void(0);"> PSello</a>. All Rights Reserved.</P>
        </div>
    </footer>
    <!-- FOOTER END -->
    @include('client.my.layouts.footer')
</body>

<!-- Mirrored from amentotech.com/htmls/psello/dashboard-post-ad.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Feb 2020 13:25:01 GMT -->

</html>
