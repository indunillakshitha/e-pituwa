
<script src="{{ asset('js/vendor/jquery.min.js')}}"></script>
<script src="{{ asset('js/vendor/popper.min.js')}}"></script>
<script src="{{ asset('js/vendor/jquery-library.js')}}"></script>
<script src="{{ asset('js/vendor/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/vendor/jquery-ui.min.js')}}"></script>
<script src="{{ asset('js/vendor/jquery.ui.touch-punch.js')}}"></script>
<script src="{{ asset('js/vendor/masonry.pkgd.min.js')}}"></script>
<script src="{{ asset('js/vendor/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{ asset('js/main.js')}}"></script>