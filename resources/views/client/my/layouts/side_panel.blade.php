 <!-- SIDEBAR START -->
 <div class="col-lg-4">
     <div class="ps-dashboard-sidebar">
         <div class="ps-dashboard-sidebar__user">
             <figure><img src="{{asset('images/insights/user-img.jpg')}}" alt="Image Description"></figure>
             <div class="ps-seller__description">
                 <h6>Lorina Statham</h6>
                 <div class="ps-h5">Status:
                     <a data-toggle="collapse" href="#collapseUser" role="button" aria-expanded="false">
                         <span><em class="ps-online"></em><i class="fa fa-sort-down"></i></span><em>Online</em>
                     </a>
                     <div class="collapse" id="collapseUser">
                         <a href="javascript:void(0);"><span class="ps-online"></span><em>Online</em></a>
                         <a href="javascript:void(0);"><span class="ps-away"></span><em>Away</em></a>
                         <a href="javascript:void(0);"><span class="ps-busy"></span><em>Busy</em></a>
                         <a href="javascript:void(0);"><span class="ps-offline"></span><em>Offline</em></a>
                     </div>
                 </div>
             </div>
             <div class="ps-dashboard-sidebar__edit"><a href="javascript:void(0);"><i class="ti-pencil"></i></a>
             </div>
         </div>
         <ul>
             <li><a href="{{route('client.insight.index')}}" class="ps-user-dhb"><i class="ti-dashboard"></i>
                     <span>Insights</span> </a></li>
             <li><a href="{{route('client.profile.index')}}"><i class="ti-user"></i> <span>Profile Settings</span>
                 </a></li>
             <li><a href="{{route('client.ads.index')}}"><i class="ti-align-justify"></i> <span>My Ads</span> </a></li>
             <li><a href="{{route('client.ads.create')}}"><i class="ti-settings"></i> <span>Post Ad</span> </a></li>
             <li><a href="dashboard-offers-messages.html"><i class="ti-email"></i> <span>Offers/messages</span>
                 </a></li>
             <li><a href="dashboard-buy-package.html" class="ps-active--line"><i class="ti-shopping-cart"></i> <span>Buy
                         New Packages</span> </a></li>
             <li><a href="dashboard-payments.html"><i class="ti-user"></i> <span>Payments</span> </a></li>
             <li>
                 <a data-toggle="collapse" href="#collapsenew" role="button" aria-expanded="false"><i
                         class="ti-heart"></i> <span>My Favorite</span> <span class="ps-right"><i
                             class="ti-angle-right"></i></span> </a>
                 <ul class="collapse" id="collapsenew">
                     <li><a href="dashboard-my-favorite.html"><span>Favorite Listing</span></a></li>
                     <li><a href="javascript:void(0);"><span>Sub Menu</span></a></li>
                 </ul>
             </li>
             <li><a href="dashboard-account-setting.html"><i class="ti-bookmark"></i> <span>Account & Privacy
                         Settings</span> </a></li>
             <li><a href="index.html"><i class="ti-shift-right"></i> <span>Logout</span> </a></li>
         </ul>
     </div>
     <div class="ps-gridList__ad ps-dashboard-img">
         <a href="javascript:void(0);">
             <figure><img src="images/ad-img.jpg" alt="Image Description"></figure>
         </a>
         <span>Advertisement 255px X 255px</span>
     </div>
 </div>
 <!-- SIDEBAR END -->
