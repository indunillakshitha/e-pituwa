@extends('app_client')

@section('content')

<!-- BANNER START -->
<div class="ps-main-banner">
    <div class="ps-dark-overlay">
        <div class="container">
            <div class="ps-banner-content">
                <h4>Dashboard</h4>
                <p><a href="index.html">Home</a> <span><i class="ti-angle-right"></i></span> Dashboard</p>
            </div>
        </div>
    </div>
</div>
<!-- BANNER END -->
<!-- MAIN START -->
<main class="ps-main">
    <section class="ps-main-section">
        <div class="container">
            <div class="row">
               @include('client.my.layouts.side_panel')
               
                 @yield('dashboard')
            </div>
        </div>
    </section>
</main>
<!-- MAIN END -->

@endsection