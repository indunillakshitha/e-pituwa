@extends('client.my.index')

@section('dashboard')

<!-- PROFILE SETTINGS START -->
<div class="col-lg-8 ps-dashboard-user">
    <div class="ps-posted-ads ps-profile-setting">
        <div class="ps-posted-ads__heading">
            <h5>Profile Settings</h5>
            <p>Click “Save Changes” to update</p>
            <button class="btn ps-btn">Save Changes</button>
        </div>
        <div class="ps-profile-setting__content">
            <form class="ps-profile-form">
                <div class="ps-profile--row">
                    <div class="form-group">
                        <input type="text" class="form-control" id="formGroupExampleInput1" required placeholder="First Name*">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="formGroupExampleInput2" required placeholder="Last Name*">
                    </div>
                    <div class="form-group">
                        <label>
                            <select class="form-control">
                                <option value="" disabled selected hidden>Select Gender:</option>
                                <option value="" >Male</option>
                                <option value="">Female</option>
                                <option value="">Other</option>
                            </select>
                        </label>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control ps-taken" required id="formGroupExampleInput3" placeholder="Username*">
                        <em>Already taken  :(</em>
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" id="formGroupExampleInput4" required placeholder="Mobile Number*">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="formGroupExampleInput5" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="formGroupExampleInput6" placeholder="Skype ID">
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" id="formGroupExampleInput7" placeholder="Whatsapp">
                    </div>
                    <div class="form-group ps-fullwidth">
                        <input type="text" class="form-control" id="formGroupExampleInput8" placeholder="Enter Logitude">
                    </div>
                    <div class="ps-profile-map ps-fullwidth">
                        <div id="ps-locationmap"></div>
                        <figure class="ps-circle">
                            <img src="images/profile-setting/img-01.png" alt="Image Description">
                        </figure>
                    </div>
                    <div class="form-group ps-fullwidth">
                        <input type="text" class="form-control" id="formGroupExampleInput9" placeholder="Enter Latitude">
                    </div>
                </div>
            </form>
            <div class="ps-profile-setting__upload">
                <h5>Profile Settings</h5>
                <div class="ps-profile-setting__uploadarea">
                    <label for="filepf">
                        <span class="btn ps-btn">Select File</span>
                        <input type="file" name="file" id="filepf" class="d-none">
                    </label>
                    <!-- <button class="btn ps-btn">Select File</button> -->
                    <p class="ps-drop">Drop files here to upload</p>
                    <div></div>
                    <p class="ps-loading">Uploading</p>
                    <svg>
                        <rect height="60px" width="100%" rx="6px"  stroke-width="2"  stroke-dasharray="12 12" />
                    </svg>
                </div>
                <form class="ps-profile-setting__imgs ps-x-axis mCustomScrollbar">
                    <input id="radio1" type="radio" name="radio">
                    <label for="radio1">
                        <span>
                            <img src="images/profile-setting/img-01.jpg" alt="Image Description">
                            <a class="ps-trash" href="javascript:void(0);"><span><i class="ti-trash"></i></span></a>
                            <span class="ps-tick"><span><i class="fas fa-check"></i></span></span>
                        </span>
                    </label>
                    <input id="radio2" type="radio" name="radio">
                    <label for="radio2">
                        <span>
                            <img src="images/profile-setting/img-02.jpg" alt="Image Description">
                            <a class="ps-trash" href="javascript:void(0);"><span><i class="ti-trash"></i></span></a> 
                            <span class="ps-tick"><span><i class="fas fa-check"></i></span></span>  
                        </span>
                    </label>
                    <input id="radio3" type="radio" name="radio">
                    <label for="radio3">
                        <span>
                            <img src="images/profile-setting/img-03.jpg" alt="Image Description">
                            <a class="ps-trash" href="javascript:void(0);"><span><i class="ti-trash"></i></span></a>  
                            <span class="ps-tick"><span><i class="fas fa-check"></i></span></span> 
                        </span>
                    </label>
                    <input id="radio4" type="radio" name="radio">
                    <label for="radio4">
                        <span>
                            <img src="images/profile-setting/img-04.jpg" alt="Image Description">
                            <a class="ps-trash" href="javascript:void(0);"><span><i class="ti-trash"></i></span></a> 
                            <span class="ps-tick"><span><i class="fas fa-check"></i></span></span>  
                        </span>
                    </label>
                    <input id="radio5" type="radio" name="radio">
                    <label for="radio5">
                        <span>
                            <img src="images/profile-setting/img-05.jpg" alt="Image Description">
                            <a class="ps-trash" href="javascript:void(0);"><span><i class="ti-trash"></i></span></a>   
                            <span class="ps-tick"><span><i class="fas fa-check"></i></span></span>
                        </span>
                    </label>
                    <input id="radio6" type="radio" name="radio">
                    <label for="radio6">
                        <span>
                            <img src="images/profile-setting/img-05.jpg" alt="Image Description">
                            <a class="ps-trash" href="javascript:void(0);"><span><i class="ti-trash"></i></span></a>   
                            <span class="ps-tick"><span><i class="fas fa-check"></i></span></span>
                        </span>
                    </label>
                </form>
            </div>
            <div class="ps-profile-setting__save">
                <button class="btn ps-btn">Save Changes</button>
                <p>Click “Save Changes” to update</p>
            </div>
        </div>
    </div>
</div>
<!-- PROFILE SETTINGS END -->


@endsection