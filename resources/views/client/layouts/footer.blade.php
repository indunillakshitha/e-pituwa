<!-- FOOTER START -->
<footer class="ps-main-footer">
    <section class="ps-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-lg-6 animate" data-animate="driveInLeft">
                    <div class="ps-footer__left">
                        <figure><img src="{{ asset('images/footer/img-01.png') }}" alt="Image Description"></figure>
                        <p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt utbor etian dolm magna aliqua
                            enim ad minim veniam quis nostrud exercita ullamco laboris nisie aliquip commodo okialama
                            sikune pisuye.</p>
                        <ul class="ps-footer__contact">
                            <li><a href="javascript:void(0);"><i class="lnr lnr-location"></i> 123 New Design Street,
                                    Melbour Australia 54214</a></li>
                            <li><a href="javascript:void(0);"><i class="lnr lnr-envelope"></i> info@domainurl.com</a>
                            </li>
                            <li><a href="javascript:void(0);"><i class="lnr lnr-phone-handset"></i> (0800) 1234
                                    567891</a></li>
                        </ul>
                        <ul class="ps-footer__social-media">
                            <li class="ps-facebook"><a href="javascript:void(0);"><i
                                        class="fab fa-facebook-f"></i></a></li>
                            <li class="ps-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li class="ps-linkedin"><a href="javascript:void(0);"><i
                                        class="fab fa-linkedin-in"></i></a></li>
                            <li class="ps-instagram"><a href="javascript:void(0);"><i
                                        class="fab fa-instagram"></i></a></li>
                            <li class="ps-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a>
                            </li>
                            <li class="ps-google-plus"><a href="javascript:void(0);"><i
                                        class="fab fa-google-plus-g"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 animate" data-animate="fadeIn">
                    <div class="ps-footer__link">
                        <h6>Useful Links</h6>
                        <ul>
                            <li><a href="javascript:void(0);">How it works?</a></li>
                            <li><a href="javascript:void(0);">About</a></li>
                            <li><a href="javascript:void(0);">Listing Grid</a></li>
                            <li><a href="javascript:void(0);">Listing Single - With Chatbox</a></li>
                            <li><a href="javascript:void(0);">Latest Article Grid</a></li>
                            <li><a href="javascript:void(0);">Latest Article Detail</a></li>
                            <li><a href="javascript:void(0);">Login Or Register</a></li>
                            <li><a href="javascript:void(0);">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 animate" data-animate="driveInRight">
                    <div class="ps-footer__newsletter">
                        <h6>Newsletter</h6>
                        <p>Eiusmod tempor incididunt utbor etian dolm magna aliqua enim minim</p>
                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="Your Email" aria-label="Your Email"
                                aria-describedby="button-addon1">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon1"><i
                                        class="ti-arrow-right"></i></button>
                            </div>
                        </div>
                        <h6>Download Mobile App</h6>
                        <div class="ps-footer__img">
                            <a href="javascript:void(0);">
                                <figure><img src="{{ asset('images/footer/img-02.png') }}" alt="Image Description">
                                </figure>
                            </a>
                            <a href="javascript:void(0);">
                                <figure><img src="{{ asset('images/footer/img-03.png') }}" alt="Image Description">
                                </figure>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="ps-footer-down animate" data-animate="fadeIn">
        <P>Copyrights © 2019 by<a href="javascript:void(0);"> PSello</a>. All Rights Reserved.</P>
    </div>
</footer>
<!-- FOOTER END -->
<script src="{{ asset('js/vendor/jquery.min.js') }}"></script>
<script src="{{ asset('js/vendor/popper.min.js') }}"></script>
<script src="{{ asset('js/vendor/jquery-library.js') }}"></script>
<script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/vendor/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/vendor/prettyPhoto.js') }}"></script>
<script src="{{ asset('js/vendor/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/vendor/jquery.ui.touch-punch.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/vendor/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{ asset('js/vendor/masonry.pkgd.min.js')}}"></script>