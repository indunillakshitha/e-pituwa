@extends('app_client')

@section('content')

<!-- BANNER START -->
<div class="ps-main-banner">
    <div class="ps-banner-img3">
        <div class="ps-dark-overlay2">
            <div class="container">
                <div class="ps-banner-contentv3">
                    <h4>Start With Categories</h4>
                    <p><a href="index.html">Home</a> <span><i class="ti-angle-right"></i></span> All Categories</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BANNER END -->
<!-- MAIN START -->
<main class="ps-main">
    <div class="container">
        <div class="ps-main-section ps-categories-details ps-center-content">
            <!-- TITLE START -->
            <div class="row">
                <div class="col-md-11 col-lg-9 col-xl-8">
                        <div class="ps-center-description">
                            <h6>Start Exploring With Our</h6>
                            <h4>Common Categories</h4>
                            <p>Consectetur adipisicing eliteiuim set eiusmod tempor incididunt labore etnalom dolore magna aliqua udiminimate veniam quistan norud.</p>
                        </div>
                    <div class="input-group">
                        <input type="email" class="form-control" placeholder="What Are You Looking For?" aria-label="Your Email" aria-describedby="button-addon1">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary ps-btn" type="button" id="button-addon1">Search Now</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TITLE END -->
            <!-- CATEGORIES START -->
            <div class="row">
                <div class="col-md-6 col-xl-4">
                    <div class="ps-categories-details__content">
                        <figure><img src="images/categories-page/icons/img-01.png" alt="Image Description"></figure>
                        <h6>Mobiles/Tablets</h6>
                    </div>
                    <ul>
                        <li><a href="javascript:void(0);">Tablets<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Accessories<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Mobile Phones<span>(12)</span></a></li>
                    </ul>
                    <div class="ps-categories-details__content">
                        <figure><img src="images/categories-page/icons/img-02.png" alt="Image Description"></figure>
                        <h6>Property For Rent</h6>
                    </div>
                    <ul>
                        <li><a href="javascript:void(0);">Houses<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Apartments & Flats<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Portions & Floors<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Shops - Offices - Commercial Space<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Roommates & Paying Guests<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Vacation Rentals - Guest Houses<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Land & Plots<span>(856)</span></a></li>
                    </ul>
                    <div class="ps-categories-details__content">
                        <figure><img src="images/categories-page/icons/img-03.png" alt="Image Description"></figure>
                        <h6>Business, Industrial And
                            Agriculture</h6>
                    </div>
                    <ul>
                        <li><a href="javascript:void(0);">Business for Sale<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Food & Restaurants<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Trade & Industrial<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Construction & Heavy Machinery<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Agriculture<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Other Business & Industry<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Medical & Pharma<span>(856)</span></a></li>
                    </ul>
                    <div class="ps-categories-details__content">
                        <figure><img src="images/categories-page/icons/img-04.png" alt="Image Description"></figure>
                        <h6>Animals</h6>
                    </div>
                    <ul>
                        <li><a href="javascript:void(0);">Fish & Aquariums<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Birds<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Hens & Aseel<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Cats<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Dogs<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Livestock<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Horses<span>(856)</span></a></li>
                        <li><a href="javascript:void(0);">Pet Food & Accessories<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Other Animals<span>(25)</span></a></li>
                    </ul>
                </div>
                <div class="col-md-6 col-xl-4">
                    <div class="ps-categories-details__content">
                        <figure><img src="images/categories-page/icons/img-05.png" alt="Image Description"></figure>
                        <h6>Vehicles</h6>
                    </div>
                    <ul>
                        <li><a href="javascript:void(0);">Cars<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Cars Accessories<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Spare Parts<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Buses, Vans & Trucks<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Other Vehicles<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Boats<span>(12)</span></a></li>
                    </ul>
                    <div class="ps-categories-details__content">
                        <figure><img src="images/categories-page/icons/img-06.png" alt="Image Description"></figure>
                        <h6>Electronics And Home
                            Appliances</h6>
                    </div>
                    <ul>
                        <li><a href="javascript:void(0);">Computers & Accessories<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">TV - Video - Audio<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Cameras & Accessories<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Games & Entertainment<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Other Home Appliances<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Generators, UPS Solutions<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Kitchen Appliances<span>(856)</span></a></li>
                        <li><a href="javascript:void(0);">AC & Coolers<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Fridges & Freezers<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Washing Machines & Dryers<span>(12)</span></a></li>
                    </ul>
                    <div class="ps-categories-details__content">
                        <figure><img src="images/categories-page/icons/img-07.png" alt="Image Description"></figure>
                        <h6>Services</h6>
                    </div>
                    <ul>
                        <li><a href="javascript:void(0);">Education & Classes<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Travel & Visa<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Car Rental<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Drivers & Taxi<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Web Development<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Other Services<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Electronics & Computer Repair<span>(856)</span></a></li>
                        <li><a href="javascript:void(0);">Event Services<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Health & Beauty<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Maids & Domestic Help<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Movers & Packers<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Home & Office Repair<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Catering & Restaurant<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Farm & Fresh Food<span>(856)</span></a></li>
                    </ul>
                </div>
                <div class="col-md-6 col-xl-4">
                    <div class="ps-categories-details__content">
                        <figure><img src="images/categories-page/icons/img-08.png" alt="Image Description"></figure>
                        <h6>Property for Sale</h6>
                    </div>
                    <ul>
                        <li><a href="javascript:void(0);">Lands<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Houses<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Apartments & Flats<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Shops, Offices And Independent Commercial Space<span>(256)</span></a></li>                           
                        <li><a href="javascript:void(0);">Portions & Floors<span>(25)</span></a></li>
                    </ul>
                    <div class="ps-categories-details__content">
                        <figure><img src="images/categories-page/icons/img-09.png" alt="Image Description"></figure>
                        <h6>Bikes And Scooters</h6>
                    </div>
                    <ul>
                        <li><a href="javascript:void(0);">Motorcycles<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Spare Parts<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Bicycles<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">ATV & Quads<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Scooters<span>(25)</span></a></li>
                    </ul>
                    <div class="ps-categories-details__content">
                        <figure><img src="images/categories-page/icons/img-10.png" alt="Image Description"></figure>
                        <h6>Jobs</h6>
                    </div>
                    <ul>
                        <li><a href="javascript:void(0);">Online<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Marketing<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Advertising & PR<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Education<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Customer Service<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Sales<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">IT & Networking<span>(856)</span></a></li>
                        <li><a href="javascript:void(0);">Hotels & Tourism<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Clerical & Administration<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Human Resources<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Accounting & Finance<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Manufacturing<span>(25)</span></a></li>
                        <li><a href="javascript:void(0);">Medical<span>(12)</span></a></li>
                        <li><a href="javascript:void(0);">Domestic Staff<span>(856)</span></a></li>
                        <li><a href="javascript:void(0);">Part - Time<span>(256)</span></a></li>
                        <li><a href="javascript:void(0);">Other Jobs<span>(25)</span></a></li>
                    </ul>
                </div>
            </div>
             <!-- CATEGORIES END -->
        </div>  
    </div>
</main>
<!-- MAIN END -->

@endsection